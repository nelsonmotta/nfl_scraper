import scrapy
import requests

key = '8otCTFFUeBNsTm6X'

def get_week(arg):
    return {
        'Hall of Fame':0,
        'Pre Wk 1':0,
        'Pre Wk 2':0,
        'Pre Wk 3':0,
        'Week 1':1,
        'Week 2':2,
        'Week 3':3,
        'Week 4':4,
        'Week 5':5,
        'Week 6':6,
        'Week 7':7,
        'Week 8':8,
        'Week 9':9,
        'Week 10':10,
        'Week 11':11,
        'Week 12':12,
        'Week 13':13,
        'Week 14':14,
        'Week 15':15,
        'Week 16':16,
        'Week 17':17,
        'Week 18':18,
        'Wild Card':19,
        'Divisional':20,
        'Conf Champ':21,
        'Super Bowl':22
    }.get(arg,None)


def get_status(arg):
    return {
        'final':0,
        'final/ot':1,
        'halftime':2,
        'end 2nd':2,
        'end 1st':3,
        'end 3rd':4,
        'delayed':5,
        'ot':6,
        '1st':7,
        '2nd':8,
        '3rd':9,
        '4th':10
    }.get(arg,12)#pregame


class NFLSpider(scrapy.Spider):
    name = "nfl_spider"
    start_urls = ['https://www.cbssports.com/nfl/scoreboard/']
     
  
    def parse(self, response):
        season = response.xpath('//div[@id="ScoreboardYearList"]/div/span/text()').get().replace('\n','').replace('Season','').strip()
        
        activeWeek = response.xpath('//div[@id="ScoreboardWeekList"]/div/span/text()').get().replace('\n','').strip()
        week = get_week(activeWeek)
        
        yield response.follow(url='https://www.cbssports.com/nfl/',callback=self.parse_carousel,meta={'season':season,'week':week})
        

    def parse_carousel(self, response):
        season = response.meta['season']
        week = response.meta['week']
        
        for match in response.xpath('//div[@id="nfl-scores-carousel"]//li'):
            possession = 'home'
            clock = '00:00'

            matchStatus = match.xpath('a/div/div[@class="current-status"]/span/text()').get().replace('\r','').replace('\n','').strip().lower(),

            if (matchStatus[0][:3] == '1st') or (matchStatus[0][:3] == '2nd') or (matchStatus[0][:3] == '3rd') or (matchStatus[0][:3] == '4th'):
                clock = matchStatus[0][3:].strip()
                matchStatus = matchStatus[0][:3]
            elif (matchStatus[0][:2] == 'ot'):
                clock = matchStatus[0][2:].strip()
                matchStatus = matchStatus[0][:2]
            else:
                matchStatus = matchStatus[0]
                
            status = get_status(matchStatus)

            awayTeamCode = match.xpath('a/div/div[contains(concat(" ",normalize-space(@class)," ")," away-team ")]/div[@class="team"]/text()').get(),
            homeTeamCode = match.xpath('a/div/div[contains(concat(" ",normalize-space(@class)," ")," home-team ")]/div[@class="team"]/text()').get(),
            awayPoints = match.xpath('a/div/div[contains(concat(" ",normalize-space(@class)," ")," away-team ")]/div[@class="score"]/text()').get().replace('\n','').strip(),
            homePoints = match.xpath('a/div/div[contains(concat(" ",normalize-space(@class)," ")," home-team ")]/div[@class="score"]/text()').get().replace('\n','').strip(),
            
            if (status == 12):
                data = dict(week=week,awayTeamCode=awayTeamCode,homeTeamCode=homeTeamCode,status=status,overUnder=awayPoints,homeTeamOdds=homePoints)
            else:
                possessionAway = match.xpath('a/div/div[@class="away-team marker"]').extract(),
                possessionHome = match.xpath('a/div/div[@class="home-team marker"]').extract(),

                if possessionAway[0]:
                    possession = 'away'
                elif possessionHome[0]:
                    possession = 'home'
                    
                data = dict(week=week,awayTeamCode=awayTeamCode,homeTeamCode=homeTeamCode,status=status,clock=clock,awayPoints=awayPoints,homePoints=homePoints,possession=possession)

            print(data)

            post_url = 'https://omega-cors-nfl.herokuapp.com/bolaonfl/matches/update/'+season+'/'+key+'/'
            print(post_url)
 #           r = requests.post(post_url,data=data)
 #           print(r.url)
 #           print(r.text)